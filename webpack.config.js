let path = require('path')
let HTMLPlugin = require('html-webpack-plugin')


module.exports =
	{ entry : './test/index.js'
	, devtool : 'inline-source-map'
	, output :
			{ path : path.resolve(__dirname, 'test/dist')
			, filename : 'bundle.test.js'
			}

	, module :
			{ rules :
					[ { test : /\.js$/
      			, exclude : /(node_modules|bower_components)/
      			, use :
      				{ loader: 'babel-loader'
      				, options: 
      					{ plugins : [require('babel-plugin-transform-function-bind')] 
        				}
      				}
    				}
					]
			}

	, plugins :
			[ new HTMLPlugin()
			]
	}