import 'rxjs/add/operator/concatMap'
import 'rxjs/add/operator/switchMap'
import 'rxjs/add/operator/delay'

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/from';

import { get_all, table, coerce_to, insert, get, _delete, pluck
       , filter, row, eq, changes, ne, update
       } from  '../dist/ReQL/Functions'

import { run, table_create } from '../dist/Driver/Web'



let query = 
	{ some_data : 'fuu'
  , term : 'START'
  , parent : null
  , args : []
  , next : null
  , opt_args : {}
  , tables : {}
  }


query::table_create('fuu')
query::table_create('filter')


let b = x => query
  :: table('fuu')
  :: insert(x)


let a = x => query
	:: table('fuu')
	:: get(x)


function get_inserted (data) {
  console.log('INSERT RESULT: ', data)
  return Observable.from(data.generated_keys.map(a).map(run))
}


let print_all = query
  :: table ('fuu')
//  :: coerce_to ('array')


let delete_all = (data) => {
  console.log('BEFORE DELETE: ', data)
  return run(query :: table('fuu') :: get(data.id) :: _delete())
}



// run(b([{ fuu : 1, bar : 2 }, { fuu : 3, bar : 4}]))
//   .concatMap(get_inserted)
//   .concatMap(o => o)
//   .concatMap(x => delete_all(x))
//   .subscribe( res => console.log('AFTER DELETE: ', res)
//             , err => console.log('QUERY ERROR: ', err))


// run(print_all).subscribe( res => console.log(res)
//                         , err => console.log(err))


let filter_delete = query
  :: table ('filter')
  :: _delete  ()


let filter_data = query
  :: table ('filter')
  :: insert ([ { title : 'One', done : true, cat : 'test' }
             , { title : 'Two', done : false, cat : 'test' }
             , { title : 'Three', done : true, cat : 'notme'}
             , { title : 'Four', done : false, cat : 'bla' }
             ])


let get_test = query
  :: table ('filter')
  :: filter (row ('cat') :: eq ('test'))
  :: coerce_to ('array')


let get_done = query
  :: table ('filter')
  :: filter (row ('done') :: eq (true))
  :: coerce_to ('array')


let get_todo = query
  :: table ('filter')
  :: filter (row ('done') :: eq (false))
  :: coerce_to ('array')


let insert_changes = query
  :: table ('filter')
  :: changes ()


let set_true = query
  :: table ('filter')
  :: filter (row ('done') :: eq (false))
  :: update ({ done : true })


let querys = run(filter_delete)
  .concatMap(log('DELETE FILTER ITEMS', filter_data))
  .concatMap(log('INSERT FILTER DATA', get_test))
  .switchMap(log('FILTER CAT EQ TEST', get_done))
  .switchMap(log('FILTER DONE EQ TRUE', get_todo))


run(insert_changes).subscribe(function (changes) {
  console.log('fuu CHANGES: ', changes)
})


setTimeout(function () {
  querys.subscribe( res => console.log('FILTER DONE EQ FALSE', res)
                  , err => console.log(err))

  setTimeout(function () {
    run(set_true).subscribe( res => console.log('UPDATE TRUE: ', res)
                           , err => console.log(err))
  }, 1000)
}, 0)



function log (label, query) {
  return function (result) {
    console.log(label, result)
    return run(query)
  }
}


function show (term) {
	if (!term) return ''
  let next = term.next ? show(term.next) : '';
  let args = term.args.map(arg => arg.show()).join(', ')
  return `[${term.term}, [${args}, ${next}], {}]`;
}


function print_query (q) {
	console.log(show(q))
}