import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map'

import { run_query } from '../../Web'
import { coerce_to } from '../Term'
import Proto from '../../../ProtoDef'


let { CHANGES } = Proto.Term.TermType


export let Changefeeds = 
	{ listen (path, fn) {
			if (undefined === this.listener[path]) this.listener[path] = []
			this.listener[path].push(fn)
		}

	
	, run (path, data) {
			(this.listener[path] || []).forEach(fn => fn(data))
		}
	
	, listener : {}
	
	}


export function register (path, query) {
	return Observable.create(observer => {
		Changefeeds.listen(path.join('.'), function (data) {
			var query_sub = run_with(query, data)
				.subscribe( change => observer.next(change)
									, err 	 => observer.error(err)
									)
		})
	})
}



function run_with (query, data) {
	let new_query = change_start(query, data)
	return run_query(new_query)
}


function change_start (query, data) {
	if (CHANGES === query.term) {
		return Start(data)
	}
	let x = Object.create(query)
	x.next = change_start(query.next, data)
	return x
}


function Start ({ data, meta }) {
	return ({ term : 'START', data, meta })
}