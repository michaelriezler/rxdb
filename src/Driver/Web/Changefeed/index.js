

import Proto from '../../../ProtoDef'
import { register } from './register'
export { Changefeeds } from './register'


let { GET, TABLE } = Proto.Term.TermType


export function changes (query) {
	let path = get_path(query)
	let start = traverse_back(remove_change(query))
	return register(path, start)
}


/*
	get_path return the path on which the changefeed should listen to.
	It returns an array strings when the array is of length 2, the query contains
	both, get and table, where get is in first position and table on the second.
	When the array is of length 1 we only have the table
*/

function get_path (query) {
	function walk (acc, x) {
		if (null === x) return acc
		if (GET === x.term) acc = acc.concat(x.args.map(i => i.value))
		if (TABLE === x.term) acc = acc.concat(x.args.map(i => i.value))
		return walk(acc, x.next)
	}
	return walk([], query)
}


function traverse_back (query) {
	while (null !== query.parent) {	
		query = query.parent
	}

	return query
}


function remove_change (change_term) {
	change_term.next = null
	return change_term
}