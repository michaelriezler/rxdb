import { Observable } from 'rxjs/Observable'



export function start (args) {
	return Observable.create(o => {
		o.next(args)
		o.complete()
	})
}