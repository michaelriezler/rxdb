// @flow

import Proto from '../../ProtoDef'
import { start } from './start'
import * as T from './Term'
import { changes } from './Changefeed'

import 'rxjs/add/operator/map'

export let table_create = T.table_create

let { DatumType } = Proto.Datum
let { TermType } = Proto.Term


export function run (query) {
		return run_query(query).map(res => res.data)
}


export function run_query (query) {
	if ('START' === query.term) return start(query)

	let { args, term, next, opt_args } = query
	next.parent = query

	if (TermType.CHANGES === query.term) return changes(query)


	let source = run_query(next)

	switch (term) {
		case TermType.TABLE : return T.table(args, query.parent, source)
		case TermType.GET : return T.get(args, source)
		case TermType.GET_ALL : return T.get_all(args, opt_args, source)
		case TermType.DELETE : return T._delete(source)
		case TermType.FILTER : return T.filter(args, source)
		case TermType.MERGE : return T.merge(args, source)
		case TermType.ORDER_BY : return T.order_by(args, source)

		case TermType.PLUCK : return T.pluck$(args, source)
		case TermType.GET_FIELD : return T.get_field$(args, source)
		case TermType.KEYS : return T.keys$(args, source)
		case TermType.VALUES : return T.values$(args, source)

		case TermType.COERCE_TO : return T.coerce_to(args, source)
		case TermType.UPDATE : return T.update(args, source)
		case TermType.TABLE_CREATE : return T.table_create(args, source)
		case TermType.INSERT : return T.insert(args, opt_args, source)
	}
}