import { Observable } from 'rxjs/Observable'


export let open = function (config) {
	return Observable.create(observer => {
		let { version
				, keyPath
				, indexes
				, autoIncrement
				, _table
				} = config

		let request = indexedDB.open(_table, version)

		request.onerror = err => {
			console.error('IDDB open error: ', err)
			observer.error(err)
		}

		request.onsuccess = event => {
				let connection = event.target.result
				let meta =
					{ meta :
							{ term : 'open' 
							, connection
							, config
							}
					}
				try {
					observer.next(meta)
					observer.complete()
				} catch (err) {
					console.log('OPEN ERROR: ', err)
					observer.error(err)
				}
		}

		request.onupgradeneeded = event => {
			let database = event.target.result
			database.onerror = observer.error

			let objectStore = database.createObjectStore(_table, { keyPath, autoIncrement })

			indexes.forEach(index => 
				objectStore.createIndex(index, index, { unique: false })
			)
		}
	})
}