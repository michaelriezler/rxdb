// @ flow

import { append, prepend, difference, set_insert, set_union, set_intersection
			 , set_difference, insert_at, splice_at, delete_at, change_at
			 , pluck, without, get_field, keys, values
			 } from '../Term'



import Proto from '../../../ProtoDef'
let { TermType } = Proto.Term


export function solve_predicate (data, pred) {
	if (null === pred.next) return solve(data, pred)
	let args = solve_predicate(data, pred.next)
	return solve(args, pred)
}


/*
 * data = data return from the previously solved predicate
 * term = current term
 * args = arguments for the current term
*/

function solve (data, { term, args }) {
	switch (term) {
		case TermType.IMPLICIT_VAR : return row(args, data)
		case TermType.EQ : return eq(args, data)
		case TermType.NE : return ne(args, data)
		case TermType.GT : return gt(args, data)
		case TermType.LT : return lt(args, data)
		case TermType.LE : return le(args, data)
		case TermType.AND : return and(args, data)
		case TermType.OR : return or(args, data)
		case TermType.DURRING : return durring(args, data)
		case TermType.IN_TIMEZONE : return in_timezone(args, data)
		
		case TermType.BRANCH : return branch(args, data)
		case TermType.ADD : return add(args, data)
		case TermType.SUB : return sub(args, data)
		case TermType.MUL : return mul(args, data)
		case TermType.DIV : return div(args, data)
		case TermType.MOD : return mod(args, data)

		case TermType.APPEND : return append(args, data)
		case TermType.PREPEND : return prepend(args, data)
		case TermType.DIFFERENCE : return difference(args, data)
		case TermType.SET_INSERT : return set_insert(args, data)
		case TermType.SET_UNION : return set_union(args, data)
		case TermType.SET_INTERSECTION : return set_intersection(args, data)
		case TermType.SET_DIFFERENCE : return set_difference(args, data)
		case TermType.INSERT_AT : return insert_at(args, data)
		case TermType.SPLICE_AT : return splice_at(args, data)
		case TermType.DELETE_AT : return delete_at(args, data)
		case TermType.CHANGE_AT : return change_at(args, data)

		case TermType.PLUCK : return pluck(args, data)
		case TermType.WITHOUT : return without(args, data)
		case TermType.GET_FIELD : return get_field(args, data)
		case TermType.KEYS : return keys(data)
		case TermType.VALUES : return values(data)
		
		default : return data
	}
}


let row = ([key], obj) => obj[key.value]

let eq = ([left], right) => left.value === get_value(right)

let ne = ([left], right) => left.value !== get_value(right)

let gt = ([left], right) => left.value > get_value(right)

let ge = ([left], right) => left.value >= get_value(right)

let lt = ([left], right) => left.value < get_value(right)

let le = ([left], right) => left.value <= get_value(right)

let not = ([left], right) => (undefined === right ? !left.value : !get_value(right))

let and = (args, other) => args.concat(other).every(x => x)

let or = (args, other) => args.concat(other).some(x => x)

let durring = () => {}

let in_timezone = () => {}

let branch = (args, other) => {
	let all = [other].concat(args)
	let last = all.length - 1

	for (let i = 0; i < last; i = i + 2) {
		if (true === solve_predicate({}, all[i])) {
			return all[i+1]
		}
	}

	return all[all.length - 1]
}


function get_value (pred) {
	if (pred && pred.next) return solve_predicate(pred.args, pred.next)
	return pred
}