import { Observable } from 'rxjs/Observable'


function next_value (meta) {
	return function (value) {
		if (null !== value) {
			value = value || {}
		}

		Object.assign(meta, { term : 'get_all' })

		return (
			{ data : value
			, meta
			}
		)
	}
}


export function get_all(source) {
	return Observable.create(observer => {
	
		function on_next ({ meta }) {
			let { connection, config } = meta

			let next_ = next_value(meta)

			let transaction = connection.transaction([config._table], 'readonly')

			let store = transaction.objectStore(config._table)

			let request = store.openCursor()

			request.onsuccess = event => {
				let cursor = event.target.result;
	      if (cursor) {
	        observer.next(next_(cursor.value))
	      	cursor.continue()
	      } else {
          observer.complete()
        }
			}
		}

		source.subscribe(on_next, observer.error, observer.complete)
	})
}