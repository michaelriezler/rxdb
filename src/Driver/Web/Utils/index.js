export { get_all } from './get_all'
export { open } from './open'
export { solve_predicate } from './solve_predicate'



export function to_object (acc, x) {
	return Object.assign(acc, x)
}