// @flow

import { open } from '../Utils'

let default_settings =
	{ version : 1
	, keyPath : 'id'
	, indexes : []
	, autoIncrement : false
	, prefix : ''
	, _table : ''
	}

export function table_create (table = 'test', user_settings = {}) {
	let settings = Object.assign({}, default_settings, { table }, user_settings)
	settings._table = settings.prefix + settings.table
	this.tables[settings.table] = settings
	return open(settings)
}