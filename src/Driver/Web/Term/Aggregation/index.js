

import { Observable } from 'rxjs/Observable'
import _ from 'lodash'

import { coerce_to } from '../coerce_to'


export function group ([field], source) {
	return Observable.create(observer => {
		coerce_to.call(source, 'array')
			.subscribe(next, err => observer.error(err), () => observer.complete())

		function next (item) {
			let data = Object
				.entries(_.groupBy(item.data, field.value))
				.map(reduction)

			Object.assign(item, { data })
			observer.next(item)
			observer.complete()
		}

		function reduction ([key, value]) {
			return { group : key, reduction : value }
		}
	})
}


export function ungroup () {

}


export function reduce ([fn], source, acc = null) {
	return Observable.create(observer => {

		function next (item) {
			if (null === acc) {
				acc = item
				return
			}

			let data = fn.args(acc.data, item.data)
			Object.assign(acc, { data })
		}

		source.subscribe(next, err => observer.error(err), () => {
			observer.next(acc)
			observer.complete()
		})
	})
}


export function fold ([start, fn], source) {
	return reduce([fn], source, start.value)
}


export function count () {

}


export function sum () {

}


export function avg () {

}


export function min () {

}


export function max () {

}


export function distinct () {

}


export function contains () {

}

