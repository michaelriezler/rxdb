import { Observable } from 'rxjs/Observable'


export function to_array(source) {
	return Observable.create(observer => {

		let result =
			{ data : []
			, meta : null
			}
			

		function on_next ({ meta, data }) {
			if (null === result.meta) {
				result.meta = meta
			}
			result.data.push(data)
		}

		source.subscribe(on_next, observer.error, () => {
			observer.next(result)
			observer.complete()
		})

	})
}


export function coerce_to ([type], source) {
	if ('array' === type.value.toLowerCase()) {
		return to_array(source)
	}
}