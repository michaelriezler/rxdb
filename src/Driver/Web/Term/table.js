// @flow


import { Observable } from 'rxjs/Observable'
import { open, get_all } from '../Utils'
import Proto from '../../../ProtoDef'
import 'rxjs/add/operator/switchMap'

let { GET, GET_ALL, INSERT } = Proto.Term.TermType


let call_open = table => ({ tables }) =>
	open(tables[table])


let call_get_all = table => ({ tables }) => {
	return get_all(open(tables[table]))
}


let should_get_all = parent =>
	( null === parent 		// parent is null or undefined
 || ( GET 		!== parent.term 
   && GET_ALL !== parent.term
   && INSERT 	!== parent.term
 ))


export function table ([table], parent, source) {
	return Observable.create(observer => {
		table = table.value

		let next = should_get_all(parent)
			? call_get_all(table)
			: call_open(table)


		source
			.switchMap(meta => next(meta))
			.subscribe(next => observer.next(next), observer.error, function () {
				observer.complete()
			})
	})
}