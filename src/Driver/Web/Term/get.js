import { Observable } from 'rxjs/Observable'


export function get ([id], source) {
	return Observable.create(observer => {

		let onSuccess = ({ meta }) => {
			let { connection, config } = meta

			try {
				let transaction =  connection.transaction([config._table], 'readonly')
				let store = transaction.objectStore(config._table)
				let request = store.get(id.value)

				request.onsuccess = () => {
					try {
						observer.next(
							{ data : request.result || {}
							, meta : 
								{ connection
								, config
								, type : 'get'
								, term : 'get'
								}
							}
						)

						observer.complete()

					} catch (err) {
						console.log('get error: ', err)
					}
				}
					
				request.onerror = observer.error
			} catch (err) {
				console.log('Get Error: ', err)
				observer.error(err)
			}
		}

		source.subscribe(onSuccess, observer.error, observer.complete)
	})
}