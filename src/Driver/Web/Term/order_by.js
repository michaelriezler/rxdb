import { Observable } from 'rxjs/Observable'
import { to_array } from './coerce_to'


export function order_by ({ index = 'id' }, source) {
	return Observable.create(observer => {

		let result =
			{ data : []
			, meta : null
			}


		function on_next (result) {
			result.data.sort((a, b) => {
				if (a[index] < b[index]) return -1
		    if(a[index] > b[index]) return 1
		    return 0
			})
			observer.next(result)
			observer.complete()
		}

		to_array.bind(source).subscribe(on_next, observer.error, observer.complete)
	})
}