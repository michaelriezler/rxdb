// @flow


import { Observable } from 'rxjs/Observable'
import { to_object } from '../../Utils'


export function without (fields : Array<any>, obj) {
	return Object.entries(obj)
		.filter(([k]) => (-1) === fields.indexOf(k))
		.map(([k, v]) => ({ [k] : v }))
		.reduce(to_object, {})
}



export function pluck$ (fields, source) {
	return Observable.create(observer => {

		function next (item) {
			let data = fields
				.map(f => f.value)
				.map(key => ({ [key] : item.data[key] }))
				.reduce(to_object, {})
			observer.next(Object.assign(item, { data }))
		}

		source.subscribe(next, err => observer.error(err), () => observer.complete())
	})
}


export function pluck (fields, obj) {
	return fields
		.map(k => k.value)
		.map(key => ({ [key] : obj[key] }))
		.reduce(to_object, {})
}


export function get_field$ ([{ value }], source) {
	return Observable.create(o => {
		source.subscribe( i => o.next(Object.assign(i, { data : i.data[value] }))
										, err => observer.error(err)
										, () => observer.complete()
										)
	})
}


export function get_field ([{ value }], obj) {
	return obj[value]
}


export function has_fields$ (fields, source) {
	return Observable.create(observer => {
		function next (item) {
			let has = fields.map(k => k.value).map(k => null !== item.data[k]).all()
			if (true === has) {
				observer.next(item)
			}
		}

		source.subscribe(next, err => observer.error(err), () => observer.complete())
	})
}


export function has_fields (fields, obj) {
	return fields.map(k => k.value).map(k => null !== item.data[k]).all()
}


export function keys$ (source) {
	return Observable.create(observer => {
		source.subscribe(next, err => observer.error(err), () => observer.complete())

		function next (item) {
			let data = Object.keys(item.data)
			Object.assign(item, { data })
			observer.next(item)
		}
	})
}


export function keys (obj) {
	return Object.keys(obj)
}


export function values$ (source) {
	return Observable.create(observer => {
		source.subscribe(next, err => observer.error(err), () => observer.complete())

		function next (item) {
			let data = Object.values(item.data)
			Object.assign(item, { data })
			observer.next(item)
		}
	})
}