// @flow


import _ from 'lodash'


export function append (item : Array<{ value : any }>, arr : Array<any>) {
	return arr.concat([item.value])
}


export function prepend ([item] : Array<{ value : any }>, arr : Array<any>) {
	return [item.value].concat(arr)
}


export function difference (without : Array<any>, arr : Array<any>) {
	return arr.filter(i => -1 === without.findIndex(x => x.value === i))
}


export function set_insert ([item], arr) {
	let a = arr.concat([item])
	return a.filter((item, pos) => pos == a.indexOf(item))
}


export function set_union (items, arr) {
	return _.union(arr, items.map(i => i.value))
}


export function set_intersection (items, arr) {
	return _.intersection(arr, item.map(i => i.value))
}


export function set_difference (items, arr) {
	return _.difference(arr, items.map(i => i.value))
}


export function insert_at ([offset, { value }], arr) {
	return arr
		.slice(0, offset.value)
		.concat([value])
		.concat(arr.slice(offset.value))
}


export function splice_at ([offset, val], arr) {
	return arr
		.slice(0, offset.value)
		.concat(val.map(v => v.value))
		.concat(arr.slice(offset.value))
}


export function delete_at (offest, arr) {
	return arr.splice.apply(null, offset.map(x => x.value))
}


export function change_at ([offset, val], arr) {
	return arr
		.slice(0, offset.value)
		.concat([val.value])
		.concat(arr.slice(offset.value + 1))
}