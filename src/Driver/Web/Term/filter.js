import { Observable } from 'rxjs/Observable'
import { row, eq } from '../../../ReQL/Functions'
import { solve_predicate } from '../Utils'

import Proto from '../../../ProtoDef'

let { MAKE_OBJECT, FUNC } = Proto.Term.TermType


let solve_predicate_object = ({ opt_args }, o) => item => {
	let [[k, v]] = Object.entries(opt_args)

	let pred = eq.call(row(k), v.value)

	if (solve_predicate(item.data, pred)) {
		o.next(item)
	}
}


let solve_function = (fn, o) => item => {
	let pred = fn(row)
	if (solve_predicate(item.data, pred)) {
		o.next(item)
	}
}


export function filter ([pred_fn], source) {
	return Observable.create(observer => {

		let next = (() => {
			if (pred_fn.term === MAKE_OBJECT) {
				return solve_predicate_object(pred_fn, observer)
			}

			if (pred_fn.term === FUNC) {
				return solve_function(pred_fn, observer)
			}

			return (item) => {
				if (solve_predicate(item.data, pred_fn)) {
					observer.next(item)
				}
			}

			console.log('Type mismatch, expected function or object, instead got', pred_fn)
		})()


		source.subscribe(next, () => observer.error(), () => observer.complete())
	})
}