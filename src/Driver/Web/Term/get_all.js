import { Observable } from 'rxjs/Observable'


let defaultConf =
	{ index : 'id' }


/*
	getAll('key_one', 'key_two', 'key_three')
	getAll('secondary_index', { index : 'fuu'})
*/


function next_value (meta) {
	return function (value) {
		if (null !== value) {
			value = value || {}
		}

		Object.assign(meta, { type : 'get_all', term : 'get_all' })

		return (
			{ data : value
			, meta
			}
		)
	}
}


function get_all_primary (args, store, next_, o) {
	/* getAll('key1', 'key2', 'key3') */
	function get_data (trx, key, rest) {
		if (undefined === key) {
			o.complete()
			return
		}

		let request = trx.get(key)

		request.onsuccess = () => {
			try {
				o.next(next_(request.result))
				get_data(trx, rest[0], rest.slice(1))
			} catch (err) {
				console.log('get error: ', err)
				o.error(err)
			}
		}
	}

	let [key, ...rest] = args
	get_data(store, key, rest)
}


function get_all_by_index (store, index, next_, o) {
	/* getAll('some_key', { index : 'not_id' }) */
	let keyRange = IDBKeyRange.only(key)
	let request = store_index.openCursor(keyRange)	

	request.onsuccess = event => {
		let cursor = event.target.result;
    if (cursor) {
    	try {
    		o.next(next_(cursor.value))
    		cursor.continue()	
    	} catch (err) {
    		console.log('get_all error: ', err)
    		o.error(err)
    	}
    } else {
   		   o.complete()
    }
	}
}

export function get_all (args, opt_args, source) {
	args = args.map(v => v.value)
	let index = opt_args.index.value

	return Observable.create(observer => {
		let call_next = ({ meta }) => {
			let { connection, config } = meta

			let next_ = next_value(meta)

			let store
			try {
				let transaction = connection.transaction([config._table], 'readonly')
				store = transaction.objectStore(config._table)
			} catch (err) {
				console.log('Transaction error: ', err)
				observer.error(err)
			}

			if ('id' === index) {
				get_all_primary(args, store, next_, observer)
			} else {
				get_all_by_index(store, index, next_, observer)
			}	
		}

		source.subscribe(call_next, observer.error)
	})
}