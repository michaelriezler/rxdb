import { Observable } from 'rxjs/Observable'
import uuid from 'uuid/v4'

import Proto from '../../../ProtoDef'
import { Changefeeds } from '../Changefeed'

let { TermType } = Proto.Term


let defaultOptions =
	{ conflict : 'error' /* || replace || update || (id -> old -> new)*/
	, returnChanges : false /* || true*/
	}


let insertOne = (store, conflict, { autoIncrement }) => data =>
	new Promise((resolve, reject) => {
			if (autoIncrement) {
				let insertData = store.put(data)			
				insertData.onsuccess = resolve
				insertData.onerror = reject
			} else {
				let getPrevData = store.get(data.id)

				getPrevData.onsuccess = () => {
					let old = getPrevData.result

					if (old && conflict === 'error') {
						reject('Error: Object already exist')
						return
					}

					
					let insertData = store.put(data)			
					insertData.onsuccess = resolve
					insertData.onerror = err => {
						console.log('Insert error: ', err)
						reject(err)
					}
				}
			}
	})


export function insert (data, opt_args = {}, source) {
	data = data.map(item => Object
		.entries(item.opt_args)
		.map(([k, v]) => [k, v.value])
		.reduce((acc, [k, v]) => Object.assign(acc, { [k] : v }), {})
	)

	return _insert(data, opt_args, source, true)
}


export function _insert (data, opt_args, source, changes) {
	return Observable.create(observer => {
		let options = Object.assign({}, defaultOptions, opt_args)
		
		let onSuccess = ({ meta }) => {
				let { connection, config } = meta
				
				let next = Next(meta)

				let store
				try {
					let transaction = connection.transaction([config._table], "readwrite")
					transaction.oncomplete = () => observer.complete()
					store = transaction.objectStore(config._table)
				} catch (err) {
					console.log('Insert Connect Error: ', err)
				}


				let sink = data
					.map(set_id)
					.map(push_id(next))
					.map(insertOne(store, options.conflict, config))


				Promise.all(sink)
					.then(_ => {
						try {
							next.data.changes = data.map(change_object)
							observer.next(next)
							
							if (true === changes) {
								run_changefeed(next.data.changes, config.table, next.meta)
							}

						} catch (e) {
							console.log('Insert All error: ', e)
							observer.error(e)
						}
					})
					.catch(err => observer.error(err))
			
			}

		source.subscribe(onSuccess, observer.error, observer.complete)
	
	})
}


function Next ({ config, connection }) {
	return (
		{ data : 
			{ changes : []
			, generated_keys : []
			}
	
		, meta : 
			{ config
			, connection
			, type : 'insert'
			}
		}
	)
}


function change_object (new_val) {
	return { old_value : null
				 , new_value : new_val
				 }
}


function push_id (next) {
	return function (obj) {
		next.data.generated_keys.push(obj.id)
		return obj
	}
}


function set_id (obj) {
	if (undefined === obj.id) obj.id = uuid()
	return obj
}


function run_changefeed (data, table, meta) {
	data.forEach(data => Changefeeds.run(table, { data, meta}))
}