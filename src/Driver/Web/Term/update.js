import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/mergeMap'

import { Changefeeds } from '../Changefeed'
import { open, to_object } from '../Utils'
import { _insert } from './insert'


import Proto from '../../../ProtoDef'
let { MAKE_OBJECT } = Proto.Term.TermType

/*
	r
		.table('Boards')
		.get(some_id)
		.update(board => (
			{ chapterPosition : board.chapterPosition.push(some_data)
			}
		))
*/

export function update ([{ opt_args }], source) {
	return Observable.create(observer => {
		
		let old_value, new_value, meta_

		let value = Object
			.entries(opt_args)
			.map(([k, v]) => ({ [k] : v.value }))
			.reduce(to_object, {})


		let update_data = ({ data, meta }) => {
			if (undefined === meta_) {
				meta_ = meta
			}

			try {
				old_value = data
				let new_data = Object.assign({}, data, value)
				new_value = new_data

				let insert_data = [new_data]
				let opt_args = { conflict : 'replace', changes : false }
				let source = open(meta.config)
				return _insert(insert_data, opt_args, source, false)
			} catch (err) {
				console.log('Error, update_data', err)
			}
		}


		let on_sucess = ({ meta }) => {
			try {
				let next = 
					{ data : 
							{ changes : [{ old_value, new_value }] }

					, meta : Object.assign(meta_, { type : 'update' })
					}

				observer.next(next)
				Changefeeds.run(meta.config.table, { data : { old_value, new_value }, meta })
			} catch (err) {
				console.log('update error', err)
			}
		}


		source
			.mergeMap(update_data)
			.subscribe(on_sucess, observer.error, observer.complete)
	})
}