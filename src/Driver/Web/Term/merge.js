import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/concatMap'


/*
	type action
		= Observable
		| Function
*/

export function merge (fn, source) {
	return Observable.create(observer => {

		function on_next (item) {
			let merge_with = fn(item.data)
			let key_val$ = Observable.from(Object.values(merge_with))

			let data = []
			let keys = []
			key_val$.concatMap(([key, observer]) => { 
					keys.push(key)
					return observer
				})
				.subscribe(item => data.push(item)
					, err => {
							console.log('merge error: ', err)
							observer.error(err)
						}

					, _ => {
							let merge_obj = keys
								.map((key, i) => ({ [key] : data[i] }))
								.reduce((acc, x) => Object.assign(acc, x), {})

							let result = Object.assign({}, item, merge_obj)
							observer.next(result)
							observer.complete()
						}
				)

		}

		source.subscribe(on_next, observer.error, observer.complete)

	})
}