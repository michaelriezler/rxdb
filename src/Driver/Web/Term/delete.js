import { Observable } from 'rxjs/Observable'

import { Changefeeds } from '../Changefeed'

/*
	r.table('Boards')
		.get(some_id)
		.delete()
*/


export function _delete (source) {
	return Observable.create(observer => {

		let result = 
			{ data : 
					{ changes : []
					}
			, meta : null
			}


		let on_next = async ({ data, meta }) => {

			if (null === result.meta) {
				result.meta = Object.assign({}, meta, { type : 'delete', 'term' : 'delete' })
			}

			let { _table } = meta.config
			let id = data[meta.config.keyPath]
			let transaction = meta.connection.transaction([_table], 'readwrite')
			let store = transaction.objectStore(_table)
			let request = store.delete(id)

			request.onsuccess = () => {
				let change = 
					{ new_value : null
					, old_value : data
					}

				result.data.changes.push(change)	
				Changefeeds.run(meta.config.table, { data : change, meta : result.meta })
			}
		}

		source.subscribe(on_next, observer.error, () => {
			observer.next(result)
			observer.complete()
		})
	})
}