// @flow

import Proto from '../ProtoDef'

let { TermType } = Proto.Term
let { DatumType } = Proto.Datum
 

export let Term = {
    create(term) {
        let self = Object.create(this)
        self.term = term
        self.args = []
        self.next = null
        self.opt_args = {}
        self.parent = null

        self.create = function (args, opt_args = {}, query = {}) {
        	let term = Object.create(this)
        	let rql = null === query ? null : Object.create(query)

        	term.args = args.map(x => expr(x))
        	term.next = rql
        	term.opt_args = Object
                .entries(opt_args)
                .map(([k, v]) => [k, expr(v)])
                .reduce((acc, [k, v]) => Object.assign(acc, { [k] : v}), {})

        	return term
        }
        
        return self
    }
}





function create_args (args) {
	let self = Object.create(this)
	self.args = args
	return self
}


export let DATUM = Term.create(TermType.DATUM)
export let MAKE_ARRAY = Term.create(TermType.MAKE_ARRAY)
export let MAKE_OBJ = Term.create(TermType.MAKE_OBJ)
export let VAR = Term.create(TermType.VAR)
export let JAVASCRIPT = Term.create(TermType.JAVASCRIPT)
export let UUID = Term.create(TermType.UUID)
export let HTTP = Term.create(TermType.HTTP)
export let ERROR = Term.create(TermType.ERROR)
export let IMPLICIT_VAR = Term.create(TermType.IMPLICIT_VAR)
export let DB = Term.create(TermType.DB)
export let TABLE = Term.create(TermType.TABLE)
export let GET = Term.create(TermType.GET)
export let GET_ALL = Term.create(TermType.GET_ALL)
export let EQ = Term.create(TermType.EQ)
export let NE = Term.create(TermType.NE)
export let LT = Term.create(TermType.LT)
export let LE = Term.create(TermType.LE)
export let GT = Term.create(TermType.GT)
export let GE = Term.create(TermType.GE)
export let NOT = Term.create(TermType.NOT)
export let ADD = Term.create(TermType.ADD)
export let SUB = Term.create(TermType.SUB)
export let MUL = Term.create(TermType.MUL)
export let DIV = Term.create(TermType.DIV)
export let MOD = Term.create(TermType.MOD)
export let FLOOR = Term.create(TermType.FLOOR)
export let CEIL = Term.create(TermType.CEIL)
export let ROUND = Term.create(TermType.ROUND)
export let APPEND = Term.create(TermType.APPEND)
export let PREPEND = Term.create(TermType.PREPEND)
export let DIFFERENCE = Term.create(TermType.DIFFERENCE)
export let SET_INSERT = Term.create(TermType.SET_INSERT)
export let SET_INTERSECTION = Term.create(TermType.SET_INTERSECTION)
export let SET_UNION = Term.create(TermType.SET_UNION)
export let SET_DIFFERENCE = Term.create(TermType.SET_DIFFERENCE)
export let SLICE = Term.create(TermType.SLICE)
export let SKIP = Term.create(TermType.SKIP)
export let LIMIT = Term.create(TermType.LIMIT)
export let OFFSETS_OF = Term.create(TermType.OFFSETS_OF)
export let CONTAINS = Term.create(TermType.CONTAINS)
export let GET_FIELD = Term.create(TermType.GET_FIELD)
export let KEYS = Term.create(TermType.KEYS)
export let VALUES = Term.create(TermType.VALUES)
export let OBJECT = Term.create(TermType.OBJECT)
export let HAS_FIELDS = Term.create(TermType.HAS_FIELDS)
export let WITH_FIELDS = Term.create(TermType.WITH_FIELDS)
export let PLUCK = Term.create(TermType.PLUCK)
export let WITHOUT = Term.create(TermType.WITHOUT)
export let MERGE = Term.create(TermType.MERGE)
export let BETWEEN_DEPRECATED = Term.create(TermType.BETWEEN_DEPRECATED)
export let BETWEEN = Term.create(TermType.BETWEEN)
export let REDUCE = Term.create(TermType.REDUCE)
export let MAP = Term.create(TermType.MAP)
export let FOLD = Term.create(TermType.FOLD)
export let FILTER = Term.create(TermType.FILTER)
export let CONCAT_MAP = Term.create(TermType.CONCAT_MAP)
export let ORDER_BY = Term.create(TermType.ORDER_BY)
export let DISTINCT = Term.create(TermType.DISTINCT)
export let COUNT = Term.create(TermType.COUNT)
export let IS_EMPTY = Term.create(TermType.IS_EMPTY)
export let UNION = Term.create(TermType.UNION)
export let NTH = Term.create(TermType.NTH)
export let BRACKET = Term.create(TermType.BRACKET)
export let INNER_JOIN = Term.create(TermType.INNER_JOIN)
export let OUTER_JOIN = Term.create(TermType.OUTER_JOIN)
export let EQ_JOIN = Term.create(TermType.EQ_JOIN)
export let ZIP = Term.create(TermType.ZIP)
export let RANGE = Term.create(TermType.RANGE)
export let INSERT_AT = Term.create(TermType.INSERT_AT)
export let DELETE_AT = Term.create(TermType.DELETE_AT)
export let CHANGE_AT = Term.create(TermType.CHANGE_AT)
export let SPLICE_AT = Term.create(TermType.SPLICE_AT)
export let COERCE_TO = Term.create(TermType.COERCE_TO)
export let TYPE_OF = Term.create(TermType.TYPE_OF)
export let UPDATE = Term.create(TermType.UPDATE)
export let DELETE = Term.create(TermType.DELETE)
export let REPLACE = Term.create(TermType.REPLACE)
export let INSERT = Term.create(TermType.INSERT)
export let DB_CREATE = Term.create(TermType.DB_CREATE)
export let DB_DROP = Term.create(TermType.DB_DROP)
export let DB_LIST = Term.create(TermType.DB_LIST)
export let TABLE_CREATE = Term.create(TermType.TABLE_CREATE)
export let TABLE_DROP = Term.create(TermType.TABLE_DROP)
export let TABLE_LIST = Term.create(TermType.TABLE_LIST)
export let CONFIG = Term.create(TermType.CONFIG)
export let STATUS = Term.create(TermType.STATUS)
export let WAIT = Term.create(TermType.WAIT)
export let RECONFIGURE = Term.create(TermType.RECONFIGURE)
export let REBALANCE = Term.create(TermType.REBALANCE)
export let SYNC = Term.create(TermType.SYNC)
export let GRANT = Term.create(TermType.GRANT)
export let INDEX_CREATE = Term.create(TermType.INDEX_CREATE)
export let INDEX_DROP = Term.create(TermType.INDEX_DROP)
export let INDEX_LIST = Term.create(TermType.INDEX_LIST)
export let INDEX_STATUS = Term.create(TermType.INDEX_STATUS)
export let INDEX_WAIT = Term.create(TermType.INDEX_WAIT)
export let INDEX_RENAME = Term.create(TermType.INDEX_RENAME)
export let FUNCALL = Term.create(TermType.FUNCALL)
export let BRANCH = Term.create(TermType.BRANCH)
export let OR = Term.create(TermType.OR)
export let AND = Term.create(TermType.AND)
export let FOR_EACH = Term.create(TermType.FOR_EACH)
export let FUNC = Term.create(TermType.FUNC)
export let ASC = Term.create(TermType.ASC)
export let DESC = Term.create(TermType.DESC)
export let INFO = Term.create(TermType.INFO)
export let MATCH = Term.create(TermType.MATCH)
export let UPCASE = Term.create(TermType.UPCASE)
export let DOWNCASE = Term.create(TermType.DOWNCASE)
export let SAMPLE = Term.create(TermType.SAMPLE)
export let DEFAULT = Term.create(TermType.DEFAULT)
export let JSON = Term.create(TermType.JSON)
export let TO_JSON_STRING = Term.create(TermType.TO_JSON_STRING)
export let TO_ISO8601 = Term.create(TermType.TO_ISO8601)
export let EPOCH_TIME = Term.create(TermType.EPOCH_TIME)
export let TO_EPOCH_TIME = Term.create(TermType.TO_EPOCH_TIME)
export let NOW = Term.create(TermType.NOW)
export let IN_TIMEZONE = Term.create(TermType.IN_TIMEZONE)
export let DURING = Term.create(TermType.DURING)
export let DATE = Term.create(TermType.DATE)
export let TIME_OF_DAY = Term.create(TermType.TIME_OF_DAY)
export let TIMEZONE = Term.create(TermType.TIMEZONE)
export let YEAR = Term.create(TermType.YEAR)
export let MONTH = Term.create(TermType.MONTH)
export let DAY = Term.create(TermType.DAY)
export let DAY_OF_WEEK = Term.create(TermType.DAY_OF_WEEK)
export let DAY_OF_YEAR = Term.create(TermType.DAY_OF_YEAR)
export let HOURS = Term.create(TermType.HOURS)
export let MINUTES = Term.create(TermType.MINUTES)
export let SECONDS = Term.create(TermType.SECONDS)
export let TIME = Term.create(TermType.TIME)
export let MONDAY = Term.create(TermType.MONDAY)
export let TUESDAY = Term.create(TermType.TUESDAY)
export let WEDNESDAY = Term.create(TermType.WEDNESDAY)
export let THURSDAY = Term.create(TermType.THURSDAY)
export let FRIDAY = Term.create(TermType.FRIDAY)
export let SATURDAY = Term.create(TermType.SATURDAY)
export let SUNDAY = Term.create(TermType.SUNDAY)
export let JANUARY = Term.create(TermType.JANUARY)
export let FEBRUARY = Term.create(TermType.FEBRUARY)
export let MARCH = Term.create(TermType.MARCH)
export let APRIL = Term.create(TermType.APRIL)
export let MAY = Term.create(TermType.MAY)
export let JUNE = Term.create(TermType.JUNE)
export let JULY = Term.create(TermType.JULY)
export let AUGUST = Term.create(TermType.AUGUST)
export let SEPTEMBER = Term.create(TermType.SEPTEMBER)
export let OCTOBER = Term.create(TermType.OCTOBER)
export let NOVEMBER = Term.create(TermType.NOVEMBER)
export let DECEMBER = Term.create(TermType.DECEMBER)
export let LITERAL = Term.create(TermType.LITERAL)
export let GROUP = Term.create(TermType.GROUP)
export let SUM = Term.create(TermType.SUM)
export let AVG = Term.create(TermType.AVG)
export let MIN = Term.create(TermType.MIN)
export let MAX = Term.create(TermType.MAX)
export let SPLIT = Term.create(TermType.SPLIT)
export let UNGROUP = Term.create(TermType.UNGROUP)
export let RANDOM = Term.create(TermType.RANDOM)
export let CHANGES = Term.create(TermType.CHANGES)
export let ARGS = Term.create(TermType.ARGS)
export let BINARY = Term.create(TermType.BINARY)
export let GEOJSON = Term.create(TermType.GEOJSON)
export let TO_GEOJSON = Term.create(TermType.TO_GEOJSON)
export let POINT = Term.create(TermType.POINT)
export let LINE = Term.create(TermType.LINE)
export let POLYGON = Term.create(TermType.POLYGON)
export let DISTANCE = Term.create(TermType.DISTANCE)
export let INTERSECTS = Term.create(TermType.INTERSECTS)
export let INCLUDES = Term.create(TermType.INCLUDES)
export let CIRCLE = Term.create(TermType.CIRCLE)
export let GET_INTERSECTING = Term.create(TermType.GET_INTERSECTING)
export let FILL = Term.create(TermType.FILL)
export let GET_NEAREST = Term.create(TermType.GET_NEAREST)
export let POLYGON_SUB = Term.create(TermType.POLYGON_SUB)
export let MINVAL = Term.create(TermType.MINVAL)
export let MAXVAL = Term.create(TermType.MAXVAL)

export let Func = Term.create(TermType.FUNC)
Func.create = create_args.bind(Func)

export let ISO8601 = Term.create(TermType.ISO8601)
ISO8601.create = create_args.bind(ISO8601)

export let Binary = Term.create(TermType.BINARY)
Binary.create = create_args.bind(Binary)

export let MakeArray = Term.create(TermType.MAKE_ARRAY)
MakeArray.create = create_args.bind(MakeArray)

export let DatumTerm = Term.create(TermType.DATUM_TERM)
DatumTerm.create = function (val) {
    let self = Object.create(this)
    self.value = val
    self.datum = (() => {
        if (null === val || undefined === null) return DatumType.R_NULL
        if (true === val || false === val) return DatumType.R_BOOL
        if (typeof val === 'number') return DatumType.R_NUM
        if (typeof val === 'string') {
            try {
                JSON.parse(val)
                return DatumType.R_JSON        
            } catch (_) {
                return DatumType.R_STR
            }
        }
        if (true === Array.isArray(val)) return DatumType.R_ARRAY
        if (typeof val == 'object') return DatumType.R_OBJECT
    })()

  	self.show = function () {
  		return `${val}`
  	}

  	return self
}


export let MakeObject = Term.create(TermType.MAKE_OBJECT)
MakeObject.create = function (obj, nesting = 20) {
	let self = Object.create(this)
	self.opt_args = {}

	for (let key of Object.keys(obj || {})) {
	    let val = obj[key];
	    if (undefined === val) {
	        // throw new err.ReqlDriverCompileError(`Object field '${key}' may not be undefined`);
	        console.log(`Object field '${key}' may not be undefined`)
	        return
	    }
	    self.opt_args[key] = expr(val, nesting - 1);
	}
	return self;
}

	
export function expr (val : any, nestingDepth : number = 20) {
    if (val === undefined) {
        //throw new err.ReqlDriverCompileError("Cannot wrap undefined with r.expr().");
        console.log("Cannot wrap undefined with r.expr().")
        return
    }

    if (nestingDepth <= 0) {
        // throw new err.ReqlDriverCompileError("Nesting depth limit exceeded.");
        console.log("Nesting depth limit exceeded.", val)
        return
    }

    if ((typeof nestingDepth !== "number") || isNaN(nestingDepth)) {
        console.log('Second argument to `r.expr` must be a number or undefined.')
        return
        // throw new err.ReqlDriverCompileError("Second argument to `r.expr` must be a number or undefined.");
    } 

    if (Term.isPrototypeOf(val)) {
        return val
    } else if (typeof val === 'function') {
        return Func.create(val)
    } else if (val instanceof Date) {
        return ISO8601.create(val.toISOString())
    } else if (val instanceof Buffer) {
        return Binary.create(val)
    } else if (Array.isArray(val)) {
        val = val.map(v => expr(v, nestingDepth - 1))
        return MakeArray.create(val)
    } else if (typeof(val) === 'number') {
        return DatumTerm.create(val);
    } else if (Object.prototype.toString.call(val) === '[object Object]') {
        return MakeObject.create(val, nestingDepth);
    } else {
        return DatumTerm.create(val);
    }
}


export function func_wrap (val) {
    if (val === undefined) {
        // Pass through the undefined value so it's caught by
        // the appropriate undefined checker
        return val;
    }

    val = expr(val)

    var var_scan = function(node) {
        if (false === Term.isPrototypeOf(node)) { 
            return false 
        }

        if (true === ImplicitVar.isPrototypeOf(node)) { 
            return true
        }

        if (true === (node.args.map(var_scan)).some(a => a)) {
            return true
        }

        let opt_args = Object
            .values(node.opt_args || {})
            .map(var_scan)
            .some(a => a)

        if (opt_args) { 
            return true 
        }

        return false
    }

    if (true === var_scan(val)) {
        return new Func({}, function(x) { return val })
    }

    return val
};