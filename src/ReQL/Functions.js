import * as T from './Term'


export let query = 
	{ term : 'START'
  , parent : null
  , args : []
  , next : null
  , opt_args : {}
  , tables : {}
  }


function no_op () {}


//Manipulating databases

// r.dbCreate(dbName) → object
export function db_create (name : string) {
	return T.DB_CREATE.create([name], {}, this)
}


// r.dbDrop(dbName) → object
export function db_drop (name : string) {
	return T.DB_DROP.create([name], {}, this)
}


// r.dbList() → array
export function db_list () {
	return T.DB_LIST.create([], {}, this)
}


// Manipulating Tables

// db.tableCreate(tableName[, options]) → object
// r.tableCreate(tableName[, options]) → object
export function table_create (name : string, opt_args) {
	let DefaultTableCreate =
		{ primary_key 				: 'id'
		, durability 					: 'soft'
		, shards 							: 1
		, replicas 						: 1
		, primary_replica_tag : {}
		}

	opt_args = Object.assign(DefaultTableCreate, opt_args)
	return T.TABLE_CREATE.create([name], opt_args, this)
}


// db.tableDrop(tableName) → object
export function table_drop (name : string) {
	return T.TABLE_DROP.create([name], {}, this)
}


// db.tableList() → array
export function table_list () {
	return T.TABLE_LIST.create([], {}, this)
}


// table.indexCreate(indexName[, indexFunction][, {multi: false, geo: false}]) → object
export function index_create (name, defun_or_opts, opts) {
	let DefaultIndexCreate =
		{ multi : false
		, geo 	: false
		}

	if (undefined !== opts) {
			opts = Object.assign(DefaultIndexCreate, opts)
	    return T.INDEX_CREATE.create([name, funcWrap(defun_or_opts)], opts, this)
	} else if (undefined !== defun_or_opts) {
	    if ( (Object.prototype.toString.call(defun_or_opts) === '[object Object]') 
	    	&& false === (typeof defun_or_opts === 'function') 
	    	&& false === T.Term.isPrototypeOf(defun_or_opts)
	    	) {
	    		defun_or_opts = Object.assign(DefaultIndexCreate, defun_or_opts)
	        return T.INDEX_CREATE.create([name], defun_or_opts, this)
	    } else {
	        return T.INDEX_CREATE.create([name, funcWrap(defun_or_opts)], DefaultIndexCreate, this)
	    }
	} else {
	    return T.INDEX_CREATE.create([name], DefaultIndexCreate, this)
	}
}


// table.indexDrop(indexName) → object
export function index_drop (name) {
	return T.INDEX_DROP.create([name], {}, this)
}


// table.indexList() → array
export function index_list () {
	return T.INDEX_LIST.create([], {}, this)
}


// table.indexRename(oldIndexName, newIndexName[, {overwrite: false}]) → object
export function index_rename (old_index, new_index, opt_args = {}) {
	let DefaultIndexRename =
		{ overwrite : false }

	opt_args = Object.assign(DefaultIndexRename, opt_args)
	return T.INDEX_RENAME.create([old_index, new_index], opt_args, this)
}


// table.indexStatus([, index...]) → array
export function index_status (...keys) {
	return T.INDEX_STATUS.create(keys, {}, this)
}


// table.indexWait([, index...]) → array
export function index_wait (...keys) {
	return T.INDEX_WAIT.create(keys, {}, this)	
}


// Writing Data

// table.insert(object | [object1, object2, ...][, {durability: "hard", returnChanges: false, conflict: "error"}]) → object
export function insert (obj, opt_args = {}) {
	let DefaultInsert = 
		{ durability 		: "hard"
		, returnChanges : false
		, conflict			: "error"
		}

	opt_args = Object.assign(DefaultInsert, opt_args)
	return Array.isArray(obj)
		? T.INSERT.create(obj, opt_args, this)
		: T.INSERT.create([obj], opt_args, this)
}


// table.update(object | function[, {durability: "hard", returnChanges: false, nonAtomic: false}]) → object
// selection.update(object | function[, {durability: "hard", returnChanges: false, nonAtomic: false}]) → object
// singleSelection.update(object | function[, {durability: "hard", returnChanges: false, nonAtomic: false}]) → object
export function update (pred_fn, opt_args = {}) {
	let DefaultUpdate =
		{ durability 		: "hard"
		, returnChanges : false
		, nonAtomic			: false
		}

	opt_args = Object.assign(DefaultUpdate, opt_args)
	return T.UPDATE.create([pred_fn], opt_args, this)
}

	
// table.replace(object | function[, {durability: "hard", returnChanges: false, nonAtomic: false}]) → object
// selection.replace(object | function[, {durability: "hard", returnChanges: false, nonAtomic: false}]) → object
// singleSelection.replace(object | function[, {durability: "hard", returnChanges: false, nonAtomic: false}]) → object
export function replace (pred_fn, opt_args = {}) {
	let DefaultUReplace =
		{ durability 		: "hard"
		, returnChanges : false
		, nonAtomic			: false
		}

	opt_args = Object.assign(DefaultUReplace, opt_args)
	return T.REPLACE.create([pred_fn], opt_args, this)
}


// table.delete([{durability: "hard", returnChanges: false}]) → object
// selection.delete([{durability: "hard", returnChanges: false}]) → object
// singleSelection.delete([{durability: "hard", returnChanges: false}]) → object
export function _delete (opt_args) {
	let DefaultDelete =
		{ durability : "hard"
		, returnChanges: false
		}

	opt_args = Object.assign(DefaultDelete, opt_args)
	return T.DELETE.create([], opt_args, this)
}


// table.sync() → object
export function sync () {
	return T.SYNC.create([], {}, this)
}


// Selecting Data

// r.db(dbName) → db
export function db (label : string) {
	return T.DB.create([label], {}, this)
}


// db.table(name[, {readMode: 'single', identifierFormat: 'name'}]) → table
export function table (label : string) {
	return T.TABLE.create([label], {}, this)
}


// table.get(key) → singleRowSelection
export function get (key : string) {
	return T.GET.create([key], {}, this)
}


// table.get(key) → singleRowSelection
export function get_all (keys : Array<string>, opt_args : { index : string } = {}) {
	opt_args = Object.assign({ index : 'id' }, opt_args)
	return T.GET_ALL.create(keys, opt_args, this)
}


// table.between(lowerKey, upperKey[, options]) → table_slice
// table_slice.between(lowerKey, upperKey[, options]) → table_slice
export function between (left, right, opt_args = {}) {
	let DefaultBetween =
		{ index 			: 'id'
		, left_bound 	: 'closed'
		, right_bound : 'open'
		}

	opt_args = Object.assign(DefaultBetween, opt_args)
	return T.BETWEEN.create([left, right], opt_args, this)
}


// selection.filter(predicate_function[, {default: false}]) → selection
// stream.filter(predicate_function[, {default: false}]) → stream
// array.filter(predicate_function[, {default: false}]) → array
export function filter (pred_function : {} | () => RQL) {
	return T.FILTER.create([pred_function], {}, this)
}


// Joins

// sequence.innerJoin(otherSequence, predicate_function) → stream
// array.innerJoin(otherSequence, predicate_function) → array
export function inner_join (sequenze, predicate_function) {
	return T.INNER_JOIN.create([sequenze, predicate_function], {}, this)
}


// sequence.outerJoin(otherSequence, predicate_function) → stream
// array.outerJoin(otherSequence, predicate_function) → array
export function outer_join (sequenze, predicate_function) {
	return T.OUTER_JOIN.create([sequenze, predicate_function], {}, this)
}


// sequence.eqJoin(leftField, rightTable[, {index: 'id', ordered: false}]) → sequence
// sequence.eqJoin(function, rightTable[, {index: 'id', ordered: false}]) → sequence
export function eq_join (left, right, opt_args = {}) {
	let DefaultEqJoin = 
		{ index 	: 'id'
		, ordered	: false
		}

	opt_args = Object.assign(DefaultEqJoin, opt_args)
	return T.EQ_JOIN.create([sequenze, predicate_function], opt_args, this)
}


// stream.zip() → stream
// array.zip() → array
export function zip () {
	return T.ZIP.create([], {}, this)
}


// Transformations 

// sequence1.map([sequence2, ...], function) → stream
// array1.map([array2, ...], function) → array
// r.map(sequence1[, sequence2, ...], function) → stream
// r.map(array1[, array2, ...], function) → array
export function map (..._args) {
	let split_args_at = Math.max(_args.length, 1)
	let args = _args.slice(0, split_args_at - 1)
	let fn = _args[split_args_at - 1]
			fn = T.func_wrap(fn)

	return T.MAP.create(args.concat(fn), {}, this)
}


// sequence.withFields([selector1, selector2...]) → stream
// array.withFields([selector1, selector2...]) → array
export function with_fields (...fields) {
	return T.WITH_FIELDS.create(fields, {}, this)
}


// stream.concatMap(function) → stream
// array.concatMap(function) → array
export function concat_map (fn) {
	return T.CONCAT_MAP.create([fn], {}, this)
}


// table.orderBy([key | function...], {index: index_name}) → table_slice
// selection.orderBy(key | function[, ...]) → selection<array>
// sequence.orderBy(key | function[, ...]) → array
export function order_by (...attrs_opts) {
	// Default if no opts dict provided
	let opts = { index: 'id' }
	let attrs = attrs_opts

	// Look for opts dict
	let perhapsOptDict = attrs_opts[attrs_opts.length - 1]
	if ( perhapsOptDict 
		&& (Object.prototype.toString.call(perhapsOptDict) === '[object Object]') 
		&& false === Term.isPrototypeOf(perhapsOptDict)
			) {
	    opts = perhapsOptDict;
	    attrs = attrs_opts.slice(0, (attrs_opts.length - 1));
	}

	attrs = attrs
		.map(x => (Asc.isPrototypeOf(x) || Desc.isPrototypeOf(x)) ? x : T.func_wrap(x))

	return T.ORDER_BY.create(attrs, opts, this)
}


// sequence.skip(n) → stream
// array.skip(n) → array
export function skip (n : number) {
	return T.SKIP.create([n], {}, this)
}


// sequence.limit(n) → stream
// array.limit(n) → array
export function limit (n : number) {
	return T.LIMIT.create([n], {}, this)
}


// selection.slice(startOffset[, endOffset, {leftBound:'closed', rightBound:'open'}]) → selection
// stream.slice(startOffset[, endOffset, {leftBound:'closed', rightBound:'open'}]) → stream
// array.slice(startOffset[, endOffset, {leftBound:'closed', rightBound:'open'}]) → array
// binary.slice(startOffset[, endOffset, {leftBound:'closed', rightBound:'open'}]) → binary
// string.slice(startOffset[, endOffset, {leftBound:'closed', rightBound:'open'}]) → string
export function slice (args : Array<number>, opt_args = {}) {
	let DefaultSlice =
		{ left_bound	:'closed'
		, right_bound :'open'
		}

	opt_args = Object.assign(DefaultSlice, opt_args)
	return T.SLICE.create(args, DefaultSlice, this)
}


// sequence.nth(index) → object
// selection.nth(index) → selection<object>
export function nth (n : number) {
	return T.NTH.create([n], {}, this)
}


export function changes (conf = {}) {
	let DefaultChanges =
		{ includ_initial : false }

	Object.assign(DefaultChanges, conf)
	return T.CHANGES.create([], DefaultChanges, this)
}


// sequence.offsetsOf(datum | predicate_function) → array
export function offsets_of (datum_pred_fn) {
	return T.OFFSETS_OF.create([T.func_wrap(datum_pred_fn)], {}, this)
}


// sequence.isEmpty() → bool
export function is_empty () {
	return T.IS_EMPTY.create([], {}, this)
}


// stream.union(sequence[, sequence, ...][, {interleave: true}]) → stream
// array.union(sequence[, sequence, ...][, {interleave: true}]) → array
// r.union(stream, sequence[, sequence, ...][, {interleave: true}]) → stream
// r.union(array, sequence[, sequence, ...][, {interleave: true}]) → array
export function union (...attrs_opts) {
	let opts = { interleave : true }
  let attrs = attrs_opts

  let perhapsOptDict = attrs_opts[attrs_opts.length - 1]

  if ( perhapsOptDict
  	&& (Object.prototype.toString.call(perhapsOptDict) === '[object Object]')
  	&& false === T.Term.isPrototypeOf(perhapsOptDicts)
  	){
      opts = perhapsOptDict
      attrs = attrs_opts.slice(0, (attrs_opts.length - 1));
  }

  attrs = attrs
  	.map(a => Asc.isPrototypeOf(a) || Desc.isPrototypeOf(a) ? a : T.func_wrap(a))

	return T.UNION.create(attrs, opts, this)
}


// sequence.sample(number) → selection
// stream.sample(number) → array
// array.sample(number) → array
export function sample (n : number) {
	return T.SAMPLE.create([n], {}, this)
}


// Aggregation


export function group (field_fn, opt_args = {}) {
	let args = T.func_wrap(field_fn)
	let DefaultGroup =
		{ index: 'id'
		, multi: false
		}

	opt_args = Object.assign(DefaultGroup, opt_args)
	return T.GROUP.create([args], opt_args, this)

}


export function ungroup () {
	return T.UNGROUP.create([], {}, this)
}


export function reduce (fn) {
	return T.REDUCE.create([T.func_wrap(fn)], {}, this)
}


export function fold (base, fn, opt_args = {}) {
	let DefaultFold = 
		{ emit 			 : no_op
		,finale_emit : no_op
		}

	opt_args = Object.assign(DefaultFold, opt_args)
	return T.FOLD.create([base, fn], opt_args, this)
}


export function count (args) {
	if (undefined === args) {
		return T.COUNT.create([], {}, this)
	}

	return T.COUNT.create([T.func_wrap(args)], {}, this)
}


export function sum (field_fn) {
	let args = T.func_wrap(field_fn)
	return T.SUM.create([args], {}, this)
}


export function avg (field_fn) {
	let args = T.func_wrap(field_fn)
	return T.AVG.create([args], {}, this)
}


export function min (field_fn) {
	let args = T.func_wrap(field_fn)
	return T.MIN.create([args], {}), this	
}


export function max (field_fn) {
	let args = T.func_wrap(field_fn)
	return T.MAX.create([args], {}, this)
}


export function distinct (pred) {
	if (undefined === pred) {
		return T.DISTINCT.create([], {}, this)
	}

	return T.DISTINCT.create([pred], {}, this)
}


export function contains (val_pred_fn) {
	let args = T.func_wrap(val_pred_fn)
	return T.DISTINCT.create([args], {}, this)
}


// Document manipulation

export function row (key) {
	return T.IMPLICIT_VAR.create([key], {}, null)
}


export function pluck (...fields) {
	return T.PLUCK.create(fields, {}, this)
}


export function without (...fields) {
	return T.WITHOUT.create(fields, {}, this)
}


export function merge (...fields) {
	let args = fields.map(T.func_wrap)
	return T.MERGE.create(args, {}, this)
}


export function append (value) {
	return T.APPEND.create([value], {}, this)
}


export function prepend (value) {
	return T.PREPEND.create([value], {}, this)
}


export function difference (values) {
	return T.DIFFERENCE.create(values, {}, this)
}


export function set_insert (value) {
	return T.SET_INSERT.create([value], {}, this)
}


export function set_union (value : Array<any>) {
	return T.SET_UNION.create(value, {}, this)
}


export function set_intersection (values : Array<any>) {
	return T.SET_INTERSECTION.create(values, {}, this)
}


export function set_difference (values : Array<any>) {
	return T.SET_DIFFERENCE.create(values, {}, this)
}


export function bracket (att : string) {
	return T.BRACKET.create([att], {}, this)
}


export function get_field (att : string) {
	return T.GET_FIELD.create([att], {}, this)
}


export function has_fields (...selectors : Array<string>) {
	return T.HAS_FIELDS.create(selectors, {}, this)
}


export function insert_at (offset : number, value) {
	return T.INSERT_AT.create([offset, value], {}, this)
}


export function splice_at (offset : number, values) {
	return T.SPLICE_AT.create([offset, values], {}, this)
}


export function delete_at (offset : number, end_offset ?: number) {
	return undefined !== end_offset
		? T.DELETE_AT.create([offset, end_offset], {}, this)
		: T.DELETE_AT.create([offset], {}, this)
}


export function change_at (offset : number, value) {
	return T.CHANGE_AT.create([offset, value], {}, this)
}


export function keys () {
	return T.KEYS.create([], {}, this)
}


export function values () {
	return T.VALUES.create([], {}, this)
}


export function literal (obj : {}) {
	return T.LITERAL.create([obj], {}, this)
}


export function object (...key_values) {
	return T.OBJECT.create(key_valus, {}, this)
}


// String manipulation

export function match (regexp) {
	return T.MATCH.create([regexp], {}, this)
}


export function split (values) {
	let args = values.slice(0, 2)
	return T.SPLIT.create(args, {}, this)
}


export function upcase () {
	return T.UPCASE.create([], {}, this)
}


export function downcase () {
	return T.DOWNCASE.create([], {}, this)
}


// Math and logic

export function add (...values) {
	return T.ADD.create(values, {}, this)
}


export function sub (...values) {
	return T.SUB.create(values, {}, this)
}


export function mul (...values) {
	return T.MUL.create(values, {}, this)
}


export function div (...values) {
	return T.DIV.create(values, {}, this)
}


export function mod (num) {
	return T.MOD.create([num], {}, this)
}


export function and (...bools) {
	return T.AND.create(bools, {}, this)
}


export function or (...bools) {
	return T.OR.create(bools, {}, this)
}


export function eq (...values) {
	return T.EQ.create(values, {}, this)
}


export function ne (...values) {
	return T.NE.create(values, {}, this)
}


export function gt (...values) {
	return T.GT.create(values, {}, this)
}


export function ge (...values) {
	return T.GE.create(values, {}, this)
}


export function lt (...values) {
	return T.LT.create(values, {}, this)
}


export function le (...values) {
	return T.LE.create(values, {}, this)
}


export function not (bool = false) {
	return T.NOT.create([bool], {}, this)
}


export function random (numbers = [], opt_args = { float : true }) {
	return T.RANDOM.create(numbers, opt_args, this)
}


export function round (num) {	
	let args = (undefined === num ? [] : [num])
	return T.ROUND.create(args, {}, this)
}


export function ceil (num) {
	let args = (undefined === num ? [] : [num])
	return T.CEIL.create(args, {}, this)
}


export function floor () {
	let args = (undefined === num ? [] : [num])
	return T.FLOOR.create(args, {}, this)
}


// Dates and times

export function now () {
	return T.NOW.create([], {}, this)
}
export function time (values : Array<number>, timezone : string) {
	return T.TIME.create(values.concat([timezone]), {}, this)
}
export function epoch_time (n : number) {
	return T.EPOCH_TIME.create([n], {}, this)
}
export function ISO8601 (time : string , opt_args = {}) {
	opt_args = Object.assign({ default_timezone : '' }, opt_args)
	return T.ISO8601.create([time], opt_args, this)
}
export function in_timezone (timezone : string) {
	return T.IN_TIMEZONE.create([timezone], {}, this)
}
export function timezone () {
	return T.TIMEZONE.create([], {}, this)
}
export function during (start, end, opt_args = {}) {
	opt_args = Object.assign({ leftBound : "closed" , rightBound : "open" }, opt_args)
	return T.DURING.create([start, end], opt_args, this)
}
export function date () {
	return T.DATE.create([], {}, this)
}
export function time_of_day () {
	return T.TIME_OF_DAY.create([], {}, this)
}
export function year () {
	return T.YEAR.create([], {}, this)
}
export function month () {
	return T.MONTH.create([], {}, this)
}
export function day () {
	return T.DAY.create([], {}, this)
}
export function day_of_week () {
	return T.DAY_OF_WEEK.create([], {}, this)
}
export function day_of_year () {
	return T.DAY_OF_YEAR.create([], {}, this)
}
export function hours () {
	return T.HOURS.create([], {}, this)
}
export function minutes () {
	return T.MINUTES.create([], {}, this)
}
export function seconds () {
	return T.SECONDS.create([], {}, this)
}
export function to_ISO8601 () {
	return T.TO_ISO8601.create([], {}, this)
}
export function to_epoch_time () {
	return T.TO_EPOCH_TIME.create([], {}, this)
}


// Control structures

export function args (args : Array<any>) {
	return T.ARGS.create(args, {}, null)
}


export function binary (data) {
	return T.BINARY.create([data], {}, null)
}


export function _do (...args) {
	args = args.map(T.func_wrap)
	return T.FUNCALL.create(args, {}, null)
}


export function branch (...args) {
	return T.BRANCH.create(args, {}, null)
}


export function for_each (fn) {
	return T.FOR_EACH.create([T.func_wrap(fn)], {}, this)
}


export function range (...range) {
	let args = range.slice(0, 2)
	return T.RANGE.create(args, {}, this)
}


export function error (msg) {
	return T.ERROR.create([msg], {}, this)
}


export function _default (...value) {
	value = value.map(T.func_wrap)
	return T.DEFAULT.create(value, {}, this)
}


export function expr (value) {
	return T.expr([value], {}, this)
}


export function js (code : string, opt_args = {}) {
	opt_args = Object.assign({ timeout : 5.0 }, opt_args) // 5 seconds
	return T.JAVASCRIPT.create([], {}, this)
}


export function coerce_to (type : string) {
	return T.COERCE_TO.create([type], {}, this)
}


export function type_of () {
	return T.TYPE_OF.create([], {}, this)
}


export function info (...value) {
	value = value.slice(0, 2)
	return T.INFO.create(value, {}, this)
}


export function json (data : string) {
	return T.JSON.create([data], {}, this)
}


export function to_JSON_string () {
	return T.TO_JSON_STRING.create([], {}, this)
}

export let to_JSON = to_JSON_string 


export function http (url, opt_args = {}) {
	opt_args = Object.assign( 
		{ timeout : 30
		, attempts : 5
		, redirects : 1
		, verify : true
		, result_format : 'auto'
		, method : 'GET'
		, auth : { type : 'basic' }
		, params : {}
		, header : 
				{ 'Accept-Encoding' : 'deflate;q=1, gzip;q=0.5'
				, 'User-Agent' : 'RethinkDB/${version}'
				}
		, data : {}
		}
	, opt_args
	)
	return T.HTTP.create([url], opt_args, this)
}


export function uuid (...value) {
	value = value.slice(0, 1)
	return T.UUID.create(value, {}, {})
}



// Geospatial commands

export function circle ([lon, lat], radius, opt_args = {}) {
	opt_args = Object.assign( { numVertices : 32, geoSystem : 'WGS84', unit : 'm', fill : true }
							 , opt_args
							 )

	return T.CIRCLE.create([lon, lat, radius], opt_args, this)
}


export function distance () {
	return T.DISTANCE.create([], {}, this)
}


export function fill () {
	return T.FILL.create([], {}, this)
}


export function geojson (obj) {
	return T.GEOJSON.create([obj], {}, this)
}


export function to_geojson () {
	return T.TO_GEOJSON.create([], {}, this)
}


export function get_intersecting (geo, index) {
	return T.GET_INTERSECTING.create([geo, index], {}, this)
}


export function get_nearest (point, opt_args) {
	opt_args = Object.assign( { max_results : 100, max_dist: 100000, unit: 'm', geo_system: 'WGS84' }
							 , opt_args
							 )

	return T.GET_NEAREST.create(point, opt_args, this)
}


export function includes (geo) {
	return T.INCLUDES.create([geo], {}, this)
}


export function intersects (...values) {
	return T.INTERSECTS.create(values, {}, this)
}


export function line (...values) {
	return T.LINE.create(values, {}, this)
}


export function point (long, lat) {
	return T.POINT.create([long, lat], {}, this)
}


export function polygon (...values) {
	return T.POLYGON.create(values, {}, this)
}


export function polygon_sub (polygon) {
	return T.POLYGON_SUB.create([polygon], {}, this)
}


// Administration

export function grant (username, permissions = {}) {
	return T.GRANT.create([username, permissions], {}, this)
}


export function config () {
	return T.CONFIG.create([], {}, this)
}


export function rebalance () {
	return T.REBALANCE.create([], {}, this)
}


export function reconfigure (config) {
	return T.RECONFIGURE.create([config], {}, this)
}


export function status () {
	return T.STATUS.create([], {}, this)
}


export function wait (opt_args = {}) {
	opt_args = Object.assign({wait_for : 'all_replicas_ready', timeout: 0 }, opt_args)
	return T.WAIT.create([], {}, this)
}

